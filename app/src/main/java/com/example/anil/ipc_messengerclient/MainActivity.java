package com.example.anil.ipc_messengerclient;

//this is working

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final int JOB_1=1;
    private static final int JOB_2=2;
    private static final int JOB_1_RESPONSE =3;
    private static final int JOB_2_RESPONSE =4;

    Messenger messenger =null;
    boolean isbind = false;
    TextView textView ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);
        ComponentName component = new ComponentName("com.example.anil.ipc_messenger","com.example.anil.ipc_messenger.MyService");

        Intent intent = new Intent();
        intent.setComponent(component);
        bindService(intent,serviceConnection,BIND_AUTO_CREATE);


    }

    @Override
    protected void onStop() {
        /*unbindService(serviceConnection);
        messenger =null;
        isbind = false;*/
        super.onStop();
    }

    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {

            messenger = new Messenger(service);
            isbind = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            messenger =null;
            isbind = false;

        }
    };

    public  void getMessage(View view )
    {
        Log.d("check","inside get message");
        textView.setText("get message called");
        String btn_text= (String)((Button)view).getText();
        if(btn_text.equals("getFirstMessage"))
        {
            Message msg = Message.obtain(null,JOB_1);
            msg.replyTo = new Messenger(new ResponseHandler());

            try {
                messenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        else if(btn_text.equals("getSecondMessage"))
        {
            Message msg = Message.obtain(null,JOB_2);
            msg.replyTo = new Messenger(new ResponseHandler());

            try {
                messenger.send(msg);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }

    }

    class ResponseHandler extends Handler
    {


        @Override
        public void handleMessage(Message msg) {
            Log.d("check","in response handler");
            String message;
            switch (msg.what)
            {
                case JOB_1_RESPONSE:
                    Log.d("check"," in response handler job1");
                    message = msg.getData().getString("response");
                    textView.setText(message);

                    break;

                case JOB_2_RESPONSE:
                    message = msg.getData().getString("response");
                    Log.d("check"," in response handler job2");
                    textView.setText(message);
                    break;

                default:
                    Log.d("check","default in response handler");

            }
            super.handleMessage(msg);
        }
    }
}
